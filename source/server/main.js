import http from "http";
import fs from "fs";
import express from "express";
import path from "path";
import request from "request";
import Markets from "./stores";


let app = express();
let filePath = 'index.html';

let confFile = fs.readFileSync(__dirname + '/config.json', 'utf8');
let config = JSON.parse(confFile)[process.env.NODE_ENV || 'development'];

let corePath = path.resolve(__dirname, "../../dist/client");

app.use(express.static(corePath)); //use static files in ROOT/public folder

app.get('/search/:quary', function(req, response, next) {
    console.log('User search: ' + req.params.quary);
    let marketsPromise = Markets().search(req.params.quary);

    marketsPromise.then((body) => {
        let result = [].concat(...body);
        result = result.sort((row1, row2) => row1.name.length - row2.name.length);
        // console.log(result);
        response.setHeader('Content-Type', 'application/json');
        response.writeHead(response.statusCode);
        response.write(JSON.stringify(result));
        response.end();
    }, (err) => {
        throw new Erorr(`error from promise: ${err}`);
    });
});

app.listen(config.PORT, config.HOST);
console.log(`run: ${config.HOST}:${config.PORT}`);