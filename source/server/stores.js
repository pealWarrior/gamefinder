import SteamStore from './markets/steamStore'
import GogStore from './markets/gogStore'

class Stores {
    constructor() {
        this.stores = {};

        this.registerStore('steam', new SteamStore());
        this.registerStore('gog', new GogStore());
    }

    registerStore(name, marketInstance) {
        if (this.stores[name])
            throw new Error('Index ' + name + ' are exist in markets!');

        this.stores[name] = marketInstance;
    }

    search(searchRequest) {
        let stores = [];
        for (let storeName in this.stores) {
            let store = this.stores[storeName];
            stores.push(store);
        }

        return Promise.all(stores.map((store) => store.search(searchRequest)));
    }
}

let instance = null;
module.exports = new function() {
    if (instance === null)
        instance = new Stores();
    return function() {
        return instance;
    };
};