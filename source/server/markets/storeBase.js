import request from "request";

const FREE_DISTRIBUTION = 'Free';
const REQUEST_TIME = 3 * 1000;

class BaseStore {
    constructor(url) {
        this._url = url;
        this._marketName = null;
    }

    set marketName(name) {
        this._marketName = name;
    }

    get marketName() {
        return this._marketName;
    }

    search(searchRequest) {
        if (this._url === undefined || this._url === null)
            throw new Error('Property _url is null or undefined');

        let onResolveFunc = (resolve, reject) => {
            request(this._url + searchRequest, { timeout: REQUEST_TIME }, (error, response, result) => {
                result = this.parseResponse(JSON.parse(result));
                resolve(result);
            });
        };

        return new Promise((resolve, reject) => onResolveFunc(resolve, reject));
    }

    /**
     * @returns @param {Array.<Object>} DataToSend
     */
    parseResponse(body) {
        throw new Error('Function parseResponse must be overrided.');
    }
}

class DataToSend {
    constructor(id, name, imageLink, metascore, urlToPage, price, marketName) {
        this.id = id;
        this.name = name;
        this.imageLink = imageLink;
        this.metascore = metascore;
        this.urlToPage = urlToPage;
        this.price = price || FREE_DISTRIBUTION;
        this.marketName = marketName;
        this.platforms = [];
        this.discount = 0;
    }

    set workedPlatforms(platforms) {
        this.platforms = platforms;
    }

    set priceDiscount(discount) {
        this.discount = discount;
    }
};

module.exports = { BaseStore, DataToSend, FREE_DISTRIBUTION };