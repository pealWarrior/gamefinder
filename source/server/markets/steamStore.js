import { BaseStore, DataToSend, FREE_DISTRIBUTION } from "./storeBase";
import request from "request";

const DATA_REQUEST_URL = 'https://store.steampowered.com/api/storesearch/?term=';
const PRODUCT_URL = (id) => `http://store.steampowered.com/app/${id}/?l=english`;
const PRODUCT_DETAIL = (id) => `https://store.steampowered.com/api/appdetails/?appids=${id}`;
const REQUEST_TIME = 2 * 1000;

class SteamStore extends BaseStore {
    constructor() {
        super(DATA_REQUEST_URL);
        this.marketName = 'steam';
    }

    parseResponse(body) { // convert/parse result from searchStoreRequest to acceptable format
        let items = body.items;
        let result = [];

        items.forEach((item) => {
            let linkToPage = PRODUCT_URL(item.id);
            let price = item.price !== undefined ?
                (parseInt(item.price.final) * 0.01).toFixed(2) + '$' :
                FREE_DISTRIBUTION;
            let dataToSend = new DataToSend(item.id, item.name, item.tiny_image, item.metascore,
                linkToPage, price, this.marketName);

            dataToSend.priceDiscount = price === FREE_DISTRIBUTION ? 0 :
                Math.floor((item.price.initial - item.price.final) / item.price.initial * 100);

            dataToSend.workedPlatforms = this.getWorkedPlatforms(item);
            result.push(dataToSend);
        });

        return result;
    }

    getWorkedPlatforms(product) {
        let result = [];

        if (product.streamingvideo)
            return result;

        for (let name in product.platforms) {
            let value = product.platforms[name];
            if (value === false) { continue; }
            result.push(name.charAt(0).toUpperCase() + name.slice(1));
        }

        return result;
    }
}

module.exports = SteamStore;