import { BaseStore, DataToSend, FREE_DISTRIBUTION } from "./storeBase";

const DATA_REQUEST_URL = 'https://www.gog.com/games/ajax/filtered?mediaType=game&sort=bestselling&search=';
const PRODUCT_URL = (url) => `https://www.gog.com${url}`;
const REQUEST_TIME = 2 * 1000;

class GogStore extends BaseStore {
    constructor() {
        super(DATA_REQUEST_URL);
        this.marketName = 'gog';
    }

    parseResponse(body) {
        let products = body.products;
        let result = [];

        products.forEach((item) => {
            let linkToPage = PRODUCT_URL(item.url);
            let linkToImage = item.image + '_196.jpg';
            let price = item.price.isFree ?
                FREE_DISTRIBUTION :
                (item.price.finalAmount + item.price.symbol);
            let dataToSend = new DataToSend(item.url, item.title, linkToImage, null, linkToPage, price, this.marketName);
            dataToSend.priceDiscount = item.price.discount;
            dataToSend.workedPlatforms = this.getWorkedPlatforms(item.worksOn);

            result.push(dataToSend);
        });

        return result;
    }

    getWorkedPlatforms(platforms) {
        let result = [];

        for (let name in platforms) {
            let value = platforms[name];
            if (value === true) { result.push(name) }
        }

        return result;
    }
}

module.exports = GogStore;