import {Injectable} from 'angular2/core'
import {Observable} from 'rxjs/Observable'
import {Http} from 'angular2/http';
import { Inject } from 'angular2/core';
import { IncomeDataStruct } from '../helpres/incomeDataStruct';

import 'rxjs/add/operator/map';

@Injectable()
export class SearchService {

    public searchedData: Observable<Array<IncomeDataStruct>>;
    private _searchObserver: any;

    constructor(@Inject(Http) private http: Http) {
        this.searchedData = new Observable(observer => this._searchObserver = observer);
    }

    search(request: string, onEndCall) { 
        // console.log('Quary is: '+request);
        this.http.get('/search/'+request)
            .map(res => res.json())
            .subscribe(
                (data:Array<Object>) => {
                    this.parseIncomeData(data);
                    onEndCall();
                },
                (error) => {throw new Error(error);}
            );
    }

    parseIncomeData(data:Array<any>) {
        let items:Array<IncomeDataStruct> = [];
        items = data.map( (instance) => {
            let result = new IncomeDataStruct();
            result.name = instance.name;
            result.imageLink = instance.imageLink;
            result.metascore = instance.metascore;
            result.urlToPage = instance.urlToPage;
            result.price = instance.price;
            result.marketName = instance.marketName;
            result.platforms = instance.platforms;
            result.discount = instance.discount;

            return result;
        });

        this._searchObserver.next(items);
    }
}