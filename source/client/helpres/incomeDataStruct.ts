export class IncomeDataStruct {
    public name: string;
    public imageLink: string;
    public metascore: number;
    public urlToPage: string;
    public price: string;
    public marketName: string;
    public platforms: Array<string>;
    public discount: string;

    constructor() {}
}