 /// <reference path="../../node_modules/angular2/typings/browser.d.ts"/>

import 'zone.js/dist/long-stack-trace-zone.js';
import 'reflect-metadata';

import {bootstrap} from 'angular2/platform/browser';
import {provide} from 'angular2/core';
import {SearchService} from './services/searchService'
import {HTTP_PROVIDERS} from 'angular2/http';

import {main} from './components/main';

bootstrap(main, [HTTP_PROVIDERS, SearchService]);
