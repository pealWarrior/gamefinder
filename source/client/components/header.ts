import {Component, Inject} from 'angular2/core';
import {SearchService} from '../services/searchService';

const DEFAULT_SEARCH_REQUEST = "empty";

@Component({
    selector: 'application',
    templateUrl: 'view/header.html'
})
export class Header {
    private width: number;
    private transDuration: number;
    constructor(@Inject(SearchService) private searchService) {
        this.width = 0;
        this.transDuration = 0.5;
    }

    search(searchQuery: string): void {
        searchQuery = searchQuery || DEFAULT_SEARCH_REQUEST;
        this.setProgress(15);

        let intervalId = setInterval(() => {
            this.setProgress(this.width + 15);

            if (this.width >= 100)
                clearInterval(intervalId);
        }, 500);

        this.searchService.search(searchQuery, () => {
            clearInterval(intervalId);
            this.onSearchDone();
        });
    }

    onSearchDone(): void {
        this.setProgress(100);
        setTimeout(() => this.forceProgress(0), 1000);
    }

    forceProgress(percent: number): void {
        this.width = percent;

        if (this.transDuration !== 0)
            this.transDuration = 0;
    }

    setProgress(percent: number): void {
        this.width = percent;

        if (this.transDuration !== 0.5)
            this.transDuration = 0.5;
    }

    onEnterPush(searchQuery: string): void {
        this.search(searchQuery);
    }
}