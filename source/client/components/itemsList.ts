import {Component, Inject} from 'angular2/core';
import {SearchService} from '../services/searchService';
import { IncomeDataStruct } from '../helpres/incomeDataStruct';

const ICONS: Object = {
    steam: "img/steam-icon.png",
    gog: "img/gog-icon.png"
};

@Component({
    selector: 'search-result',
    templateUrl: 'view/list.html',
    styleUrls: ['css/product.css']
})
export class ItemsList {
    public currentData: Array<IncomeDataStruct> = [];
    public storesIcons: Object;

    constructor(@Inject(SearchService) private SearchService) {
        this.SearchService.searchedData.subscribe(nextData => this.currentData = nextData);
        this.storesIcons = ICONS;
    }
}