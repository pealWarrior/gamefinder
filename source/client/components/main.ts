import {Component} from 'angular2/core';

import {Header} from './header';
import {ItemsList} from './itemsList';

@Component({
    directives: [Header, ItemsList],
    selector: 'gamefinder',
    templateUrl: 'view/main.html'
})
export class main {
    constructor() {
        console.log('Create');
    }
}