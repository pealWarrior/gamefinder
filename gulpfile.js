'use strict';

var fs = require('fs');
var gulp = require('gulp'),
    g_less = require('gulp-less'),
    g_util = require('gulp-util'),
    g_html = require('gulp-html-minifier'),
    g_babel = require('gulp-babel'),
    browserify = require('browserify'),
    tsify = require('tsify'),
    source = require('vinyl-source-stream');

var env = g_util.env.env || 'development';
var config = JSON.parse(fs.readFileSync('./gulpconfig.json'))[env];

gulp.task('babel', () => {
    return gulp.src([
            'source/server/**/*.js', 'source/server/**/*.json'
        ])
        .on('error', function(err) {
            g_util.log(g_util.colors.red.bold('[browserify error]'));
            g_util.log(err.message);
            this.emit('end');
        })
        .pipe(g_babel(config.babel))
        .pipe(gulp.dest('dist/server/'))
});

gulp.task('browserify', () => {
    return browserify(config.browserify)
        .plugin(tsify, {
            target: 'es5',
            experimentalDecorators: true
        })
        .bundle()
        .on('error', function(err) {
            g_util.log(g_util.colors.red.bold('[browserify error]'));
            g_util.log(err.message);
            this.emit('end');
        })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('./dist/client'));
});

gulp.task('less', function() {
    return gulp.src('source/client/less/**/*.less')
        .pipe(g_less())
        .on('error', function(err) {
            // Handle less errors, but do not stop watch task
            g_util.log(g_util.colors.red.bold('[Less error]'));
            g_util.log(g_util.colors.bgRed('filename:') + ' ' + err.filename);
            g_util.log(g_util.colors.bgRed('lineNumber:') + ' ' + err.lineNumber);
            g_util.log(g_util.colors.bgRed('extract:') + ' ' + err.extract.join(' '));
            this.emit('end');
        })
        .pipe(gulp.dest('./dist/client/css'))
});

gulp.task('html', function() {
    return gulp.src('./source/client/**/*.html')
        .pipe(g_html(config.html))
        .on('error', function(err) {
            // Handle less errors, but do not stop watch task
            g_util.log(g_util.colors.red.bold('[Less error]'));
            g_util.log(g_util.colors.bgRed('filename:') + ' ' + err.filename);
            g_util.log(g_util.colors.bgRed('lineNumber:') + ' ' + err.lineNumber);
            g_util.log(g_util.colors.bgRed('extract:') + ' ' + err.extract.join(' '));
            this.emit('end');
        })
        .pipe(gulp.dest('./dist/client'));
});

gulp.task('watch_client', () => {
    gulp.watch('./source/client/**/*.ts', ['browserify']);
    gulp.watch('./source/client/less/*.less', ['less']);
    gulp.watch('./source/client/view/*.html', ['html']);
});

gulp.task('watch_server', () => {
    gulp.watch('./source/server/**/*.js', ['babel']);
});


gulp.task('client', ['html', 'browserify', 'less']);
gulp.task('client_w', ['client', 'watch_client']);

gulp.task('server', ['babel']);
gulp.task('server_w', ['server', 'watch_server']);